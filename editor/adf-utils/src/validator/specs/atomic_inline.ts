export default [
  'hardBreak',
  'mention',
  'emoji',
  'inlineExtension',
  'date',
  'placeholder',
  'inlineCard',
  'status',
];
