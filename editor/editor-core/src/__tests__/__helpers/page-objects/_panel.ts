import {
  PanelSharedCssClassName,
  PanelSharedSelectors,
} from '@atlaskit/editor-common';

export const panelSelectors = {
  panel: `.${PanelSharedCssClassName.prefix}`,
  infoPanel: PanelSharedSelectors.infoPanel,
};
