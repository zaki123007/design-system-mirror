import { FileState, FileDetails } from '@atlaskit/media-client';

export const extendMetadata = (
  state: FileState,
  metadata?: FileDetails,
): FileDetails => {
  const { id } = state;
  const currentMediaType = metadata && metadata.mediaType;
  if (state.status !== 'error') {
    return {
      id,
      name: state.name,
      size: state.size,
      mimeType: state.mimeType,
      createdAt: state.createdAt,
      // We preserve the initial mediaType
      // in case file subscription returns 'unknown'
      // while it's uploading/processing
      mediaType:
        currentMediaType && currentMediaType !== 'unknown'
          ? currentMediaType
          : state.mediaType,
    };
  } else {
    return {
      id,
    };
  }
};
