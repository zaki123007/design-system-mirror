import styled from 'styled-components';

export const ImageComponent = styled.img`
  align-self: center;
  object-fit: contain;
`;
