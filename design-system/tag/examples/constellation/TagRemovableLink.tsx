import React from 'react';
import Tag from '../../src';

export default () => (
  <Tag
    text="Removable tag link"
    removeButtonText="Remove"
    href="/components/tag"
  />
);
